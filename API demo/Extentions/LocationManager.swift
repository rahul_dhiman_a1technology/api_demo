//  API demo
//
//  Created by Rahul Dhiman on 5/25/17.
//  Copyright © 2017 Rahul Dhiman. All rights reserved.
//
import UIKit
import Foundation
import UIKit
import CoreLocation
//import PermissionScope

struct Location : CustomStringConvertible {
    var current_lat : String?
    var current_lng : String?
    var current_formattedAddr : String?
    var current_city : String?
    
    var description: String{
        return CommonFunctions.appendOptionalStrings(withArray: [current_formattedAddr])
    }
}

struct DeviceTime {
    
    var currentTime : String {
        get{
            return  localTimeFormatter.string(from: NSDate() as Date).lowercased()
        }
    }
    
    var currentDate : String{
        get{
            return localDateFormatter.string(from: Date())
        }
    }
    
    var current_zone : String {
        get {
            return TimeZone.autoupdatingCurrent.identifier
        }
    }
    
    
    
    func convertCreated_at (_ created_at : String?) -> Date?{
        return localDateTimeFormatter.date(from: created_at ?? "")
    }
    
    
    var localTimeZoneFormatter = DateFormatter()
    var localTimeFormatter = DateFormatter()
    var localDateFormatter = DateFormatter()
    
    var localDateTimeFormatter = DateFormatter()
    
    func setUpTimeFormatters(){
        localTimeFormatter.dateFormat = "hh:mm a"
        localDateFormatter.dateFormat = "dd MMM, yyyy"
        localDateTimeFormatter.dateFormat = "dd MMM, yyyy hh:mm a Z"
        
    }
//    "sent_date": "15 Aug, 2016",
//    "sent_time": "01:00 AM"
}

class LocationManager: NSObject ,  CLLocationManagerDelegate  {
    
    private static var __once: () = {
            Static.instance = LocationManager()
        }()
    
    class var sharedInstance: LocationManager {
        struct Static {
            static var instance: LocationManager?
            static var token: Int = 0
        }
        _ = LocationManager.__once
        return Static.instance!
    }
    var currentTime : DeviceTime? = DeviceTime()

    var currentLocation : Location? = Location(){
        didSet{
            let user = UserDataSingleton.sharedInstance.loggedInUser
            user?.location = LocationManager.sharedInstance.currentLocation?.current_formattedAddr
//            user?.latitude = LocationManager.sharedInstance.currentLocation?.current_lat
//            user?.longitude = LocationManager.sharedInstance.currentLocation?.current_lng
//            UserDataSingleton.sharedInstance.loggedInUser = user
//            HTTPServer.sharedInstance.opertationWithRequest(withApi: API.UserAPI(user: API.User.UpdateLocation())) { (response) in
//            }
        }
    }
    let locationManager = CLLocationManager()
//     let pscope = PermissionScope()
    
    func startTrackingUser(){
        // For use in foreground
        currentTime?.setUpTimeFormatters()

        
        /*
        pscope.permissionButtonBorderColor = Colors.GreenColor.color()
        pscope.closeButtonTextColor = UIColor.blackColor()
        pscope.permissionButtonTextColor = Colors.GreenColor.color()
        pscope.addPermission(LocationWhileInUsePermission(),
                             message: L10n.WeUseThisToFindBetterDealsAroundYou.string)
        // Show dialog with callbacks
        pscope.show({ finished, results in
            self.locationManager.delegate = self
            self.locationManager.desiredAccuracy = 100
            self.locationManager.startUpdatingLocation()
            }, cancelled: { (results) -> Void in
                print("thing was cancelled")
        })
         */
        
    
    }
    
}

extension LocationManager{
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        if status == CLAuthorizationStatus.authorizedWhenInUse || status == CLAuthorizationStatus.authorizedAlways {
            locationManager.startUpdatingLocation()
        }
        
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error.localizedDescription)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        guard let firstLocation = locations.first else{return}
        CLGeocoder().reverseGeocodeLocation(firstLocation) {[unowned self] (placemarks, error) in
            self.currentLocation?.current_lat = "\(firstLocation.coordinate.latitude)"
            self.currentLocation?.current_lng = "\(firstLocation.coordinate.longitude)"
            guard let bestPlacemark = placemarks?.first else{return}
            self.currentLocation?.current_city = bestPlacemark.locality
            self.currentLocation?.current_formattedAddr = CommonFunctions.appendOptionalStrings(withArray: [bestPlacemark.subThoroughfare , bestPlacemark.thoroughfare , bestPlacemark.locality , bestPlacemark.country])
            self.locationManager.stopUpdatingLocation()
            self.locationManager.delegate = nil
        
        }
        
    }

}
