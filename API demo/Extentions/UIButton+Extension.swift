//  API demo
//
//  Created by Rahul Dhiman on 5/25/17.
//  Copyright © 2017 Rahul Dhiman. All rights reserved.
//
import Foundation
import UIKit

extension UIButton {
    
    
    class func animateLike (_ sender : UIButton?){
        
        let pulseAnimation:CABasicAnimation = CABasicAnimation(keyPath: "transform.scale")
        pulseAnimation.duration = 0.2
        pulseAnimation.fromValue = 1.0
        pulseAnimation.toValue = NSNumber(value: 1.4 as Float)
        pulseAnimation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        pulseAnimation.autoreverses = true
        pulseAnimation.repeatCount = 0
        sender?.layer.add(pulseAnimation, forKey: nil)
    }
}
 /*
    func likeDislike(_ isLike : Bool , likeButton : UIButton , dislikeButton : UIButton , feed : Feed? , completion : ()->()){
        
        if !Reachability.isConnectedToNetwork(){
            UIApplication.shared.keyWindow?.makeToast("Please check your internet connection")
            return
        }
        
        if isLike{
            
            if ¿feed?.isLiked == TRUE ||  ¿feed?.isDisliked == TRUE{
                if let count = feed?.dislikeCount?.toInt(),  ¿feed?.isDisliked == TRUE{
                    feed?.dislikeCount = count == 0 ? "\(0)" : "\(count - 1)"
                }
                
                if let count = feed?.likesCount?.toInt(),  ¿feed?.isLiked == TRUE{
                    feed?.likesCount = count == 0 ? "\(0)" : "\(count - 1)"
                }
                
                feed?.isLiked = FALSE
                feed?.isDisliked = FALSE
                
                completion()
                // Unlike Un dislike
                HTTPServer.sharedInstance.opertationWithRequest(withApi: API.postAPI(post: API.Post.unLikeDislike(post_avp_id: feed?.postId)), completion: { (response) in })
                
            }
            else if ¿feed?.isLiked == FALSE{
                feed?.isLiked = TRUE
                feed?.likesCount = "\((feed?.likesCount?.toInt() ?? 1) + 1)"
                UIButton.animateLike(likeButton)
                completion()
                // Liked
                HTTPServer.sharedInstance.opertationWithRequest(withApi: API.postAPI(post: API.Post.like(postAvgId: feed?.postId)), completion: { (response) in })
                
                
            }
            
        }
        else{
            
            if ¿feed?.isLiked == TRUE ||  ¿feed?.isDisliked == TRUE{
                
                if let count = feed?.dislikeCount?.toInt(),  ¿feed?.isDisliked == TRUE{
                    feed?.dislikeCount = count == 0 ? "\(0)" : "\(count - 1)"
                }
                
                if let count = feed?.likesCount?.toInt(),  ¿feed?.isLiked == TRUE{
                    feed?.likesCount = count == 0 ? "\(0)" : "\(count - 1)"
                }
                feed?.isLiked = FALSE
                feed?.isDisliked = FALSE
                completion()
                // Unlike Un dislike
                
                HTTPServer.sharedInstance.opertationWithRequest(withApi: API.postAPI(post: API.Post.unLikeDislike(post_avp_id: feed?.postId)), completion: { (response) in })
                
            }
            else if ¿feed?.isLiked == FALSE{
                feed?.isDisliked = TRUE
                feed?.dislikeCount = "\((feed?.dislikeCount?.toInt() ?? 0) + 1)"
                completion()
                // Disliked
                UIButton.animateLike(dislikeButton)
                HTTPServer.sharedInstance.opertationWithRequest(withApi: API.postAPI(post: API.Post.dislike(postAvgId: feed?.postId)), completion: { (response) in })
                
            }
        }
        
    }
*/
