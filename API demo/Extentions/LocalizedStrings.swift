// Generated using SwiftGen, by O.Halligon — https://github.com/AliSoftware/SwiftGen
//  API demo
//
//  Created by Rahul Dhiman on 5/25/17.
//  Copyright © 2017 Rahul Dhiman. All rights reserved.
//
import Foundation

enum L10n {
  /// Please enter your email address
  case pleaseEnterYourEmailAddress
  /// Please enter your password
  case pleaseEnterYourPassword
  /// Please enter a valid email
  case pleaseEnterAValidEmail
  /// Your session has expired. Please login to continue!
  case yourSessionHasExpiredPleaseLoginToContinue
  /// Please check your internet connection!
  case pleaseCheckYourInternetConnection
  /// Cancel
  case cancel
  /// Send
  case send
  /// Change Password
  case changePassword
  /// Old Password
  case oldPassword
  /// New Password
  case newPassword
  /// Confirm Password
  case confirmPassword
  /// Email
  case email
  /// Firstname
  case firstname
  /// Lastname
  case lastname
  /// Comments • 
  case comments
  /// Posted
  case posted
  /// Please enter a detailed message
  case pleaseEnterADetailedMessage
  /// Please enter a title
  case pleaseEnterATitle
  /// Please select a category
  case pleaseSelectACategory
  /// Please enter a description
  case pleaseEnterADescription
  /// Please select atleast 1 image
  case pleaseSelectAtleast1Image
  /// Please select a video
  case pleaseSelectAVideo
  /// Category
  case category
  /// Please enter a valid url
  case pleaseEnterAValidUrl
  /// You can upload maximum of 5 images
  case youCanUploadMaximumOf5Images
  /// Camera
  case camera
  /// Photo Library
  case photoLibrary
  /// Please enter a valid old password
  case pleaseEnterAValidOldPassword
  /// Please enter a valid new password
  case pleaseEnterAValidNewPassword
  /// Please confirm your password correctly
  case pleaseConfirmYourPasswordCorrectly
  /// Please enter a valid firstname
  case pleaseEnterAValidFirstname
  /// Please enter a valid lastname
  case pleaseEnterAValidLastname
}

extension L10n: CustomStringConvertible {
  var description: String { return self.string }

  var string: String {
    switch self {
      case .pleaseEnterYourEmailAddress:
        return L10n.tr("Please enter your email address")
      case .pleaseEnterYourPassword:
        return L10n.tr("Please enter your password")
      case .pleaseEnterAValidEmail:
        return L10n.tr("Please enter a valid email")
      case .yourSessionHasExpiredPleaseLoginToContinue:
        return L10n.tr("Your session has expired. Please login to continue!")
      case .pleaseCheckYourInternetConnection:
        return L10n.tr("Please check your internet connection!")
      case .cancel:
        return L10n.tr("Cancel")
      case .send:
        return L10n.tr("Send")
      case .changePassword:
        return L10n.tr("Change Password")
      case .oldPassword:
        return L10n.tr("Old Password")
      case .newPassword:
        return L10n.tr("New Password")
      case .confirmPassword:
        return L10n.tr("Confirm Password")
      case .email:
        return L10n.tr("Email")
      case .firstname:
        return L10n.tr("Firstname")
      case .lastname:
        return L10n.tr("Lastname")
      case .comments:
        return L10n.tr("Comments • ")
      case .posted:
        return L10n.tr("Posted")
      case .pleaseEnterADetailedMessage:
        return L10n.tr("Please enter a detailed message")
      case .pleaseEnterATitle:
        return L10n.tr("Please enter a title")
      case .pleaseSelectACategory:
        return L10n.tr("Please select a category")
      case .pleaseEnterADescription:
        return L10n.tr("Please enter a description")
      case .pleaseSelectAtleast1Image:
        return L10n.tr("Please select atleast 1 image")
      case .pleaseSelectAVideo:
        return L10n.tr("Please select a video")
      case .category:
        return L10n.tr("Category")
      case .pleaseEnterAValidUrl:
        return L10n.tr("Please enter a valid url")
      case .youCanUploadMaximumOf5Images:
        return L10n.tr("You can upload maximum of 5 images")
      case .camera:
        return L10n.tr("Camera")
      case .photoLibrary:
        return L10n.tr("Photo Library")
      case .pleaseEnterAValidOldPassword:
        return L10n.tr("Please enter a valid old password")
      case .pleaseEnterAValidNewPassword:
        return L10n.tr("Please enter a valid new password")
      case .pleaseConfirmYourPasswordCorrectly:
        return L10n.tr("Please confirm your password correctly")
      case .pleaseEnterAValidFirstname:
        return L10n.tr("Please enter a valid firstname")
      case .pleaseEnterAValidLastname:
        return L10n.tr("Please enter a valid lastname")
    }
  }

  fileprivate static func tr(_ key: String, _ args: CVarArg...) -> String {
    let format = NSLocalizedString(key, comment: "")
    return String(format: format, locale: Locale.current, arguments: args)
  }
}

func tr(_ key: L10n) -> String {
  return key.string
}
