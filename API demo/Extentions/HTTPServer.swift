//  API demo
//
//  Created by Rahul Dhiman on 5/25/17.
//  Copyright © 2017 Rahul Dhiman. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import EZSwiftExtensions

typealias APICompletion = (APIResponse) -> ()

class HTTPServer {
    
    
    static let sharedInstance = HTTPServer()
    fileprivate lazy var httpClient : HTTPClient = HTTPClient()
    func opertationWithRequest (withApi api : API , completion : @escaping APICompletion )  {
        print(api.parameters)
        print(api.url())
        if isLoaderNeeded(api){
            CommonFunctions.showLoader()
        }
        
        httpClient.postRequest(withApi: api, success: { (data) in
            CommonFunctions.hideLoader()
            guard let response = data else {
                completion(APIResponse.failure(.None))
                return
            }

            print(response )
            
            let json = JSON(response)
//            print(json)
            
            if let errorMessage = json["error_description"].string, errorMessage.characters.count > 0 {
                
//                switch api{
//                case .nemesisAPI(let value):
//                    switch value{
//                    case .battleWinner(_):
//                        return
//                    default: break
//                    }
//                default: break
//                }
                return
            }
            
            let object : AnyObject?
            object = api.handleParameters(json)
            completion(APIResponse.success(object))
            return
            
            
            }, failure: { (message) in
                print(message)
                completion(APIResponse.failure(.None))
                // UIApplication.shared.keyWindow?.makeToast(L10n.pleaseCheckYourInternetConnection.string)
                CommonFunctions.hideLoader()
        })
        
    }
       
    
    func opertationWithRequestMedia (withApi api : API , withVideo url : URL? , images : [UIImage]?  , completion : @escaping APICompletion )  {
        
        
        print(api.parameters)
        
        if isLoaderNeeded(api){
            CommonFunctions.showLoader()
        }
        
        httpClient.postRequestMedia(withApi: api, video: url! , images : images , success: { (data) in
            CommonFunctions.hideLoader()
            guard let response = data else {
                completion(APIResponse.failure(.None))
                return
            }
            let json = JSON(response)
            
            if let errorMessage = json["error_description"].string, errorMessage.characters.count > 0 {
//                UIApplication.sharedApplication().keyWindow?.makeToast(errorMessage)
                return
            }
                
            let object : AnyObject?
            object = api.handleParameters(json)
            completion(APIResponse.success(object))
            return
            
            }, failure: { (message) in
                print(message)
                
             
//                UIApplication.shared.keyWindow?.makeToast(L10n.pleaseCheckYourInternetConnection.string)
                CommonFunctions.hideLoader()
        })
        
    }
    
    
    func isLoaderNeeded(_ api : API) -> Bool{
        
              
        switch api {
            
        case .userAPI(let value):
            
            switch value {
            case .login(_,_):
                return true
            default:
                return false
            }
//        case .postAPI(let value):
//            
//            switch value {
//            case .dislike(_) , .like(_) , .unLikeDislike(_) , .postNoViews(_) , .reportPost(_) :
//                return false
//                
//            default:
//                return true
//            }
//    
//
//        case .nemesisAPI(_):
//            return false
            
        }
    }
    
}




protocol Router {
//  var route : String { get }
    var baseURL : String { get }
    var parameters : OptionalDictionary { get }
    var headers : OptionalDictionary { get }
   // var method : Alamofire.Method { get }
}


enum API {
   static func mapKeysAndValues(_ keys : [String]?,values : [String]?) -> [String : String]?{
        guard let tempValues = values,let tempKeys = keys else { return nil}
        var params = [String : String]()
        for (key,value) in zip(tempKeys,tempValues) {
            params[key] = ¿value  // 
        }
        return params
    }


    
    enum User{
        case login(email : String? , password : String?)
        case signUp(email : String? , password : String? , fname : String? , lname : String?)
        case forgotPassword(email : String?) // TBD
        
        func formatParameters() -> OptionalDictionary {

            let sessionId = ""
            switch self {
        
            case .login(let email , let password):
                return API.mapKeysAndValues(APIParameterConstants.User.Login , values: [ "login" , ¿email , ¿password , "" , "" , "" , ""])
        
            case .signUp(let email , let password , let fname , let lname):
               return API.mapKeysAndValues([""], values: [ "signup" , ¿email , ¿password , ¿fname , ¿lname , "" , "" , "", ""])
                
            default :
                return nil
//
//            case .forgotPassword(let email):
//                return API.mapKeysAndValues(APIParameterConstants.User.ForgotPassword , values: [ "forgot_password" , ¿email])
                //
            }
            
        }
        
    }
    
    case userAPI(user : User)
    
    
    func handleParameters(_ parameters : JSON?) -> AnyObject? {
        
        let dictResponse = parameters?.dictionaryValue
        let arrayResponse = parameters?.arrayValue
        switch self {
        case .userAPI(let value):
            switch value {
            case .login(_)  :
                let user = UserModal(withAttributes: dictResponse)
                return user
            default : return "" as AnyObject
        }
        
//        case .sidePanelAPI(let value) :
//            switch value {
//            case .home(_) , .memeCategory(_) , .trendingCategory(_) , .videoCategory(_) , .hotRNotCategory(_) , .imagesCategory(_):
//                let feeds = Feed.getFeed(withJSONArray: arrayResponse)
//                return feeds
//
//            default:
//                return "" as AnyObject
//            }
        
            default:
                return nil
            }
        
        }
}



extension API : Router {
    
//    API KEY
//    SECRET
//    f7d16b882f050b93567bd8b903e4c03d1eb259f9

    var headers: OptionalDictionary{
        
        switch self {
 
        default:
            return nil
        }
        
    }
    
    var parameters: OptionalDictionary{
    
        switch self {
        case .userAPI(let value): return value.formatParameters()
        }
        
    }
    
    
    var method : Alamofire.HTTPMethod {
        switch self {
            
        default:
            return .post
        }
    }
    
    var baseURL: String{
        return APIConstants.BasePath
    }
    
    
    func url() -> String {
        
        switch self {
        
        default:
            return baseURL
        }
        
    }
}
