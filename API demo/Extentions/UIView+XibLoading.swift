//  API demo
//
//  Created by Rahul Dhiman on 5/25/17.
//  Copyright © 2017 Rahul Dhiman. All rights reserved.
//

import Foundation
import UIKit


enum XibError : Error {
    case xibNotFound
    case none
}



extension UIView {
    
    func loadViewFromNib(withIdentifier identifier : String) throws -> UIView? {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName:identifier, bundle: bundle)
        let xibs = nib.instantiate(withOwner: self, options: nil)
        
        if xibs.count == 0 {
            return nil
        }
        guard let firstXib = xibs[0] as? UIView else{
            throw XibError.xibNotFound
        }
        return firstXib
    }
    
}
