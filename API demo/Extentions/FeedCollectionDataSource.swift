//  API demo
//
//  Created by Rahul Dhiman on 5/25/17.
//  Copyright © 2017 Rahul Dhiman. All rights reserved.
//

import UIKit

typealias CollectionViewCellConfigureBlock = (_ cell:UICollectionViewCell, _ item:AnyObject?) -> ()
typealias CollectionViewCellDidSelect = (_ indexpath:IndexPath) -> ()

class FeedCollectionDataSource : NSObject,UICollectionViewDataSource,UICollectionViewDelegate {
    
    
    
    var items :[Any] = []
    var itemIdentifier:String?
    var configureCellBlock:CollectionViewCellConfigureBlock?
    var aRowSelectedListener:CollectionViewCellDidSelect?
    
    init(items: NSArray, cellIdentifier: String, configureBlock: @escaping CollectionViewCellConfigureBlock,onRowSelect: @escaping CollectionViewCellDidSelect) {
        self.items = items as! [Any]
        self.itemIdentifier = cellIdentifier
        self.configureCellBlock = configureBlock
        self.aRowSelectedListener = onRowSelect
        super.init()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if (items.count>6){
            return 6
        }
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: self.itemIdentifier!, for: indexPath) as UICollectionViewCell
        let item: AnyObject = self.itemAtIndexPath(indexPath)
        
        if (self.configureCellBlock != nil) {
            self.configureCellBlock!(cell, item)
        }
        
        return cell
    }
    
    func itemAtIndexPath(_ indexPath: IndexPath) -> AnyObject {
        return self.items[indexPath.row] as AnyObject
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        
        if (self.aRowSelectedListener != nil) {
            self.aRowSelectedListener!(indexPath)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                               sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.width)}
    
    
    
    
}
