//  API demo
//
//  Created by Rahul Dhiman on 5/25/17.
//  Copyright © 2017 Rahul Dhiman. All rights reserved.
//

import UIKit



class CollectionViewDataSource: NSObject  {
    
    var items : Array<AnyObject>?
    var cellIdentifier : String?
    var headerIdentifier : String?
    var collectionVw  : UICollectionView?
    var cellHeight : CGFloat = 0.0
    var cellWidth : CGFloat = 0.0
    var isServiceTypeCollectionView : Bool = false
    
    
    var configureCellBlock : ListCellConfigureBlock?
    var aRowSelectedListener : DidSelectedRow?

    init (items : Array<AnyObject>?  , collctionView : UICollectionView? , cellIdentifier : String? , headerIdentifier : String? , cellHeight : CGFloat , cellWidth : CGFloat  , configureCellBlock : @escaping ListCellConfigureBlock  , aRowSelectedListener : @escaping DidSelectedRow)  {
        
        self.collectionVw = collctionView
        self.items = items
        self.cellIdentifier = cellIdentifier
        self.headerIdentifier = headerIdentifier
        self.cellWidth = cellWidth
        self.cellHeight = cellHeight
        self.configureCellBlock = configureCellBlock
        self.aRowSelectedListener = aRowSelectedListener
        
    }
    
    override init() {
        super.init()
    }
    
}

extension CollectionViewDataSource : UICollectionViewDelegate , UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let identifier = cellIdentifier else{
            fatalError("Cell identifier not provided")
        }
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier ,
                                                                         for: indexPath) as UICollectionViewCell
        if let block = self.configureCellBlock , let item: AnyObject = self.items?[indexPath.row]{
            block(cell , item)
        }
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.items?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let block = self.aRowSelectedListener{
            block(indexPath)
        }
    }

}


extension CollectionViewDataSource : UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if isServiceTypeCollectionView && indexPath.row == (items?.count ?? 0) - 1  {
            let layout = collectionViewLayout as! UICollectionViewFlowLayout
            switch (items?.count ?? 0) % 3 {
            case 1 :
                return CGSize(width: collectionView.bounds.width - (layout.sectionInset.left + layout.sectionInset.right ) , height: cellHeight)
            case 2 :
                return CGSize(width: (cellWidth * 2) + 8.0 , height: cellHeight)
            default:
                return CGSize(width: cellWidth, height: cellHeight)

            }
        }
        return CGSize(width: cellWidth, height: cellHeight)
    }
}

/*
extension CollectionViewDataSource : UIScrollViewDelegate{
    
    func scrollViewWillEndDragging(scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
        guard let layout = self.tableView?.collectionViewLayout as? UICollectionViewFlowLayout where scrollView.contentOffset.y < 0 else{return}
        let cellWithIncludingSpace = tableViewRowWidth + layout.minimumLineSpacing
        var offset = targetContentOffset.memory
        let index = round((scrollView.contentInset.left + offset.x)/cellWithIncludingSpace)
        offset = CGPointMake(index * cellWithIncludingSpace  - scrollView.contentInset.left - (2 * layout.minimumLineSpacing) , -scrollView.contentInset.top)
        targetContentOffset.memory = offset        
    }
    
}
 */
