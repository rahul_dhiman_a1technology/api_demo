//  API demo
//
//  Created by Rahul Dhiman on 5/25/17.
//  Copyright © 2017 Rahul Dhiman. All rights reserved.
//

import UIKit

typealias TextFieldReturn = (_ textField : UITextField) -> ()

class TextFieldDataSource: NSObject {

    var textField : UITextField?
    var textFieldReturnBlock : TextFieldReturn?
    var viewController : UIViewController?
    
    init(textField : UITextField,sender : UIViewController) {
        super.init()
        self.textField = textField
        self.viewController = sender
        self.textField?.delegate = self
    }
    
    override init() {
        super.init()
    }
}

extension TextFieldDataSource : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        viewController?.view.endEditing(true)
        return true
        guard let block = textFieldReturnBlock else {
            return false
        }
        block(textField)
        return true
    }
}

