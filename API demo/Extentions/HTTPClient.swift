//  API demo
//
//  Created by Rahul Dhiman on 5/25/17.
//  Copyright © 2017 Rahul Dhiman. All rights reserved.
//
import Foundation
import Alamofire
import AVFoundation

typealias HttpClientSuccess = (AnyObject?) -> ()
typealias HttpClientFailure = (String) -> ()


enum HttpStatusCode : Int {
    case ok = 200
    case nonResponse = 202
    case badRequest = 400
    case unAuthorized = 401
    case forbidden = 403
    case notFound = 404
    case jsonError = 3840
    
    func title () -> String{
        
        switch self {
        case .ok:
            return "Api Success"
        case .nonResponse :
            return "Request was accepted , but there was no response"
        case .badRequest :
            return "Swift at fault here"
        case .unAuthorized , .forbidden :
            return "Your session was expired , please login to continue!"
        default:
            return "Invalid json from serverr"
        }
        
    }
}

class HTTPClient {
    
    func JSONObjectWithData(_ data: Data) -> Any? {
        do { return try JSONSerialization.jsonObject(with: data, options: []) }
        catch { return .none }
    }
    
    func postRequest(withApi api : API  , success : @escaping HttpClientSuccess , failure : @escaping HttpClientFailure )  {
        
        let params = api.parameters
        let fullPath = api.url()
        let method = api.method
        
        
      /*  Alamofire.request(method, fullPath, parameters: params, encoding: .URL, headers: api.headers)
            .responseJSON { (response) in
                                
                if let httpError = response.result.error {
                    let statusCode = HttpStatusCode(rawValue: httpError.code)
                    failure(statusCode?.title() ?? "")
                }
                else{
                    
                    let statusCode = (response.response?.statusCode) ?? HttpStatusCode.UnAuthorized.rawValue
                    if statusCode == HttpStatusCode.UnAuthorized.rawValue || statusCode == HttpStatusCode.Forbidden.rawValue {
                        GiFHUD.dismiss()
                        // Session Expired Handler
                        print("\(statusCode)")
                        return
                    }
                    switch response.result {
                    case .Success(let data):
                        success(data)
                    case .Failure(let error):
                        failure(error.localizedDescription)
                    }
                }
        } */
        
    }

    func randomStringWithLength (_ len : Int) -> NSString {
        
        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let randomString : NSMutableString = NSMutableString(capacity: len)
        for _ in (0..<len) {
            let length = UInt32 (letters.length)
            let rand = arc4random_uniform(length)
            randomString.appendFormat("%C", letters.character(at: Int(rand)))
        }
        return randomString
    }
    
    func postRequestMedia(withApi api : API,video : URL? , images : [UIImage]? , success : @escaping HttpClientSuccess , failure : @escaping HttpClientFailure){
        
        guard let params = api.parameters else {failure("empty"); return}
        let fullPath = APIConstants.BasePath
        let method = api.method
        /*
        Alamofire.upload(
            method,
            fullPath,
            multipartFormData: { multipartFormData in
                
                
                switch api{
                case .PostAPI(let value):
                    
                    switch value {
                    case .PostGoViral(_,_,let type,_):
                        
                       if type == PostType.Video{
                            guard let videoUrl = video else {failure("empty");return}
                            multipartFormData.appendBodyPart(fileURL: videoUrl, name: "path")
                            guard let image = CommonFunctions.generateThumnail(videoUrl) , let data = UIImageJPEGRepresentation(image, 0.0) else {break}
                        multipartFormData.appendBodyPart(data: data, name: "video_thumb", fileName: "\(self.randomStringWithLength(10))" + ".jpg" ,mimeType: "image/jpg")

                        }
                        else if type == PostType.TextPhoto || type == PostType.Article{
                            var index = 1
                            for image in images ?? []{
                                guard let data = UIImageJPEGRepresentation(image, 0.0) else {break}
                                multipartFormData.appendBodyPart(data: data, name: "path" + "_\(index)" , fileName: "path" + "_\(index).jpg" ,mimeType: "image/jpg")
                                index = index + 1
                            }
                        }

                    case .AddComment(_,_ , let type):
                        if type == CommentType.Picture{
                            guard let imageCount = images?.count, imageCount > 0 else {failure("empty");return}
                            guard let data = UIImageJPEGRepresentation(images![0], 0.0) else {failure("empty");return}
                            multipartFormData.appendBodyPart(data: data, name: "path", fileName: "temp.jpg" ,mimeType: "image/jpg")
                        }
                        else if type == CommentType.Video{
                            guard let videoUrl = video else {failure("empty");return}
                            multipartFormData.appendBodyPart(fileURL: videoUrl, name: "path")
                            guard let image = CommonFunctions.generateThumnail(videoUrl) , let data = UIImageJPEGRepresentation(image, 0.0) else {break}
                            multipartFormData.appendBodyPart(data: data, name: "video_thumb", fileName: "\(self.randomStringWithLength(10))" + ".jpg" ,mimeType: "image/jpg")

                        }
                    default:
                        break
                    }
                    
                case .UserAPI(let user):
                    
                    switch user {
                    case .EditProfile(_):
                        
                        guard let imageCount = images?.count, imageCount > 0 else {failure("empty");return}
                        guard let data = UIImageJPEGRepresentation(images![0], 0.0) else {failure("empty");return}
                        multipartFormData.appendBodyPart(data: data, name: "profilepic", fileName: "\(self.randomStringWithLength(10))" + ".jpg" ,mimeType: "image/jpg")

                        
                    default : break
                        
                    }
                    

                    default : break
                }
                
                
                
                for (key, value) in params {
//                    if value.characters.count == 0{
//                        continue
//                    }
                    multipartFormData.appendBodyPart(data: value.dataUsingEncoding(NSUTF8StringEncoding)!, name: key)
                }
                
            },
            encodingCompletion: { encodingResult in
                switch encodingResult {
                case .Success(let upload,_,_):
                    upload.progress { (bytesWritten, totalBytesWritten, totalBytesExpectedToWrite) in
                        print(totalBytesWritten)
                    }
                    upload.responseString(completionHandler: { (response) in
                        print(response)
                    })
                    upload.responseJSON { response in
                        switch response.result {
                        case .Success(let data):
                            success(data)
                        case .Failure(let error):
                            failure(error.localizedDescription)
                        }
                    }
                case .Failure(let encodingError):
                    failure(encodingError.toString)
                }
            }
        )
       */
    }
}
