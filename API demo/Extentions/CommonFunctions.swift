//  API demo
//
//  Created by Rahul Dhiman on 5/25/17.
//  Copyright © 2017 Rahul Dhiman. All rights reserved.
//

import Foundation
import DropDown
import SystemConfiguration
import AVFoundation
import EZSwiftExtensions
import DropDown

open class Reachability {
    class func isConnectedToNetwork() -> Bool {
        
        var zeroAddress = sockaddr_in(sin_len: 0, sin_family: 0, sin_port: 0, sin_addr: in_addr(s_addr: 0), sin_zero: (0, 0, 0, 0, 0, 0, 0, 0))
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags: SCNetworkReachabilityFlags = SCNetworkReachabilityFlags(rawValue: 0)
        if SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) == false {
            return false
        }
        
        /* Only Working for WIFI
         let isReachable = flags == .reachable
         let needsConnection = flags == .connectionRequired
         
         return isReachable && !needsConnection
         */
        
        // Working for Cellular and WIFI
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        let ret = (isReachable && !needsConnection)
        
        return ret
        
    }
}


public struct DocumentDirectory {
    
    static func path() -> AnyObject {
        
        let paths = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
        let documentsDirectory: AnyObject = paths[0] as AnyObject
        return documentsDirectory
        
    }
    
}



class CommonFunctions {
    
    class func showDropDown(_ dropDown : DropDown , sender : UIButton? , values : [String] , onSelection : @escaping (String) -> ()){
        
        
        if let sender = sender {
            dropDown.anchorView = sender
            dropDown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height+2)
        }
        dropDown.dataSource = values
        dropDown.backgroundColor = UIColor(white: 1, alpha: 1)
        //dropDown.cornerRadius = 10
        dropDown.shadowColor = UIColor(white: 0.6, alpha: 1)
        dropDown.shadowOpacity = 0.9
        dropDown.shadowRadius = 25
        dropDown.animationduration = 0.25
        dropDown.textColor = .darkGray
        dropDown.selectionAction = { (index, item) in
            onSelection(item)
        }
        
        
    }

    
    class func generateThumnail(_ video : URL) -> UIImage?{
        
        do {
            let asset = AVURLAsset(url: video , options: nil)
            let imgGenerator = AVAssetImageGenerator(asset: asset)
            imgGenerator.appliesPreferredTrackTransform = true
            let cgImage = try imgGenerator.copyCGImage(at: CMTimeMake(0, 1), actualTime: nil)
            let uiImage = UIImage(cgImage: cgImage)
            
            return uiImage
            // lay out this image view, or if it already exists, set its image property to uiImage
        } catch let error as NSError {
            print("Error generating thumbnail: \(error)")
            return nil
        }
        
    }
    
    class func showLoader(){
        
       

    }

    class func hideLoader(){
    }
    
    class func appendOptionalStrings(withArray array : [String?]) -> String {
        
        return array.flatMap{$0}.joined(separator: " ")
    }
    
    class func appendOptionalStrings(withArray array : [String?],separatorString : String) -> String {
        
        return array.flatMap{$0}.joined(separator: separatorString)
    }
    
 
    
    class func shareContentOnSocialMedia (withViewController controller : UIViewController? ){
        let  activityController = UIActivityViewController(activityItems: ["Hey! Check out this new application Viral and let us know your reviews"], applicationActivities: [])
        
        let arrExcludedTypes = [UIActivityType.airDrop,UIActivityType.print,
                                UIActivityType.assignToContact,
                                UIActivityType.saveToCameraRoll,
                                UIActivityType.addToReadingList,
                                UIActivityType.postToFlickr,
                                UIActivityType.postToVimeo]
        
        activityController.excludedActivityTypes = arrExcludedTypes
        controller?.present(activityController, animated: true, completion: nil)
    }
    
    
    
    class func shareContentOnSocialMedia (withViewController controller : UIViewController? , title : String , url : URL?){
        
        var items :[AnyObject] = [title as AnyObject]
        if let tempUrl = url {
            items.append(tempUrl as AnyObject)
        }
        
        let  activityController = UIActivityViewController(activityItems: items, applicationActivities: [])
        
        let arrExcludedTypes = [UIActivityType.airDrop,UIActivityType.print,
                                UIActivityType.assignToContact,
                                UIActivityType.saveToCameraRoll,
                                UIActivityType.addToReadingList,
                                UIActivityType.postToFlickr,
                                UIActivityType.postToVimeo]
        
        activityController.excludedActivityTypes = arrExcludedTypes
        controller?.present(activityController, animated: true, completion: nil)
    
    }
    
    
    class func showDropDown(_ dropDown : DropDown , sender : UIButton , values : [String] , onSelection : @escaping (String) -> ()){
        
            dropDown.anchorView = sender
            dropDown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height+2)
            dropDown.dataSource = values
            dropDown.backgroundColor = UIColor(white: 1, alpha: 1)
            //dropDown.cornerRadius = 10
            dropDown.shadowColor = UIColor(white: 0.6, alpha: 1)
            dropDown.shadowOpacity = 0.9
            dropDown.shadowRadius = 25
            dropDown.animationduration = 0.25
            dropDown.textColor = .darkGray
            dropDown.selectionAction = { (index, item) in
                onSelection(item)
        }

        
    }
 
    
    
   
}


