//  API demo
//
//  Created by Rahul Dhiman on 5/25/17.
//  Copyright © 2017 Rahul Dhiman. All rights reserved.
//
import Foundation
import SwiftyJSON
import RMMapper



protocol JSONDecodable  {
    init(withAttributes attributes : OptionalSwiftJSONParameters)
}




enum  UserKeys : String {
    case session_id = "session_id"
    case gender = "gender"
    case location = "location"
    case lastname = "lastname"
    case firstname = "firstname"
    case email = "email"
    case dob = "dob"
    case picture = "picture"
    case push_notification = "push_notification"
    case user_id = "user_id"
    case name = "name"
    case picture_base_path = "picture_base_path"
    case is_following = "is_following"
    case followers = "followers"
    case following = "followings"
}


class UserModal : RMMapper , RMMapping , JSONDecodable {
    
    var sessionId : String?
    var gender : String?
    var location : String?
    var lastname : String?
    var firstname : String?
    var email : String?
    var dob : String?
    var picture : String?
    var pushNotification : String?
    var user_id : String?
    var is_following : String = "0"
    var followers : String = "0"
    var following : String = "0"
  
    override init() {
        super.init()
    }


    required init(withAttributes attributes: OptionalSwiftJSONParameters)  {
        
        let profile = attributes?["profile"]?.dictionaryValue
        self.gender = UserKeys.gender.rawValue => profile
        self.location = UserKeys.location.rawValue => profile
        self.lastname = UserKeys.lastname.rawValue => profile
        self.firstname = UserKeys.firstname.rawValue => profile

//        self.followers = UserKeys.followers.rawValue => profile ?? "0"
//        self.following = UserKeys.following.rawValue => profile ?? "0"
//        self.is_following = UserKeys.is_following.rawValue => profile ?? "0"

        
        self.dob = UserKeys.dob.rawValue => profile
        self.email = UserKeys.email.rawValue => profile
        self.user_id = UserKeys.user_id.rawValue => profile
        self.picture =  CommonFunctions.appendOptionalStrings(withArray: [ UserKeys.picture_base_path.rawValue => profile , UserKeys.picture.rawValue => profile ], separatorString: "")
        
        
        self.pushNotification = UserKeys.push_notification.rawValue => attributes
        self.sessionId = UserKeys.session_id.rawValue => attributes

    }
    
    
    required init(withoutProfileAttributes attributes: OptionalSwiftJSONParameters)  {
        
        self.gender = UserKeys.gender.rawValue => attributes
        self.location = UserKeys.location.rawValue => attributes
        self.lastname = UserKeys.lastname.rawValue => attributes
        self.firstname = UserKeys.firstname.rawValue => attributes
        
//        self.followers = UserKeys.followers.rawValue => attributes ?? "0"
//        self.following = UserKeys.following.rawValue => attributes ?? "0"
//        self.is_following = UserKeys.is_following.rawValue => attributes ?? "0"
//        
        
        self.dob = UserKeys.dob.rawValue => attributes
        self.email = UserKeys.email.rawValue => attributes
        self.user_id = UserKeys.user_id.rawValue => attributes
        self.picture =  CommonFunctions.appendOptionalStrings(withArray: [ UserKeys.picture_base_path.rawValue => attributes , UserKeys.picture.rawValue => attributes ], separatorString: "")
        
        
        self.pushNotification = UserKeys.push_notification.rawValue => attributes
        self.sessionId = UserKeys.session_id.rawValue => attributes
        
    }
    
}



extension UserModal {
    
    
    
    class func getUsers(withJSONArray array : [JSON]?) -> [UserModal]? {
        
        let rawUsers = array ?? []
        var parsedUsers = [UserModal]()
        for tempU in rawUsers{
            let user = UserModal(withAttributes: tempU.dictionaryValue)
            
            print(tempU.dictionaryValue)
            user.firstname = UserKeys.firstname.rawValue => tempU.dictionaryValue
            user.picture =  CommonFunctions.appendOptionalStrings(withArray: [ UserKeys.picture_base_path.rawValue => tempU.dictionaryValue , UserKeys.picture.rawValue => tempU.dictionaryValue ], separatorString: "")
            user.user_id = UserKeys.user_id.rawValue => tempU.dictionaryValue
            user.is_following = (UserKeys.is_following.rawValue => tempU.dictionaryValue) ?? "0"
            user.following = (UserKeys.following.rawValue => tempU.dictionaryValue) ?? "0"
            user.followers = (UserKeys.followers.rawValue => tempU.dictionaryValue) ?? "0"
            
            parsedUsers.append(user)
        }
        return parsedUsers
    }
    
}



/*
 
 "location" : "Santa Clara",
 "following" : "1",
 "is_following" : "1",
 "lastname" : "ds",
 "firstname" : "sd",
 "picture" : "images.jpg",
 "followers" : "1",
 "user_id" : "1",
 "picture_base_path" : "http://enter_image_base_url_here"
 */
