//  API demo
//
//  Created by Rahul Dhiman on 5/25/17.
//  Copyright © 2017 Rahul Dhiman. All rights reserved.
//

import Foundation
import SwiftyJSON


infix operator => {associativity left precedence 160}
infix operator =| {associativity left precedence 160}


typealias OptionalSwiftJSONParameters = [String : JSON]?

func =>(key : String, json : OptionalSwiftJSONParameters) -> String?{
    return json?[key]?.stringValue
}
func =|(key : String, json : OptionalSwiftJSONParameters) -> [JSON]?{
    return json?[key]?.arrayValue
}



prefix operator ¿
prefix func ¿(value : String?) -> String {
    return value.unwrap()
}


internal struct APIParameterConstants {
    
    struct User {
        
        static let Login =
            [
                "event" ,
                "email",
                "password" ,
                "device_type" ,
                "device_id",
                "device_token",
                "debug"
            ]
        static let Logout =
            [
                "event",
                "session_id"
            ]
    }
    
    
    /*
    
    struct ChatParams {
        static let ChatSend = [
            "event",
            "session_id",
            "user_id",
            "message"
        ]
        
        static let ChatReceiver = [
            "event",
            "session_id",
            "user_id",
            "old",
            "page",
            "timezone"
        ]
    }
 
 */
    
}

internal struct APIConstants {
    
    static let BasePath = "http://viralapp.bluebullpharma.com/api/v2/index.php"
    static let DataKey = "data"
    static let status = "success"
    static let message = "message"
    
    static let ApiKey = "45691832"
    static let SecretKey = "f7d16b882f050b93567bd8b903e4c03d1eb259f9"
}

//private struct 

enum APIValidation : String {
    
    case None
    case Success = "1"
    case Failure = "0"
    case InvalidAccessToken = "2"
    
    func mapResponseMessage(_ message : String?) -> String? {
        
        switch self {
        case .Success:
            return message
            
        case .Failure :
            return message
        case .InvalidAccessToken :
            return message
    
        default:
            return nil
        }

    }

}

enum APIResponse {
    case success(AnyObject?)
    case failure(APIValidation)
}

typealias OptionalDictionary = [String : String]?








