//
//  ViewController.swift
//  API demo
//
//  Created by Rahul Dhiman on 5/25/17.
//  Copyright © 2017 Rahul Dhiman. All rights reserved.
//

import UIKit
import EZSwiftExtensions

class ViewController: UIViewController {
    @IBOutlet var tfEmail: UITextField!
    @IBOutlet var tfPass: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func actionLogin(_ sender: AnyObject) {
        
        HTTPServer.sharedInstance.opertationWithRequest(withApi: API.userAPI(user: API.User.login(email: tfEmail.text, password: tfPass.text))) {[unowned self] (response) in
            switch response{
                
            case .failure(_):
                break
            case .success(_):
                ez.runThisInMainThread({
                   // self.presentVC(StoryboardScene.Home.instantiateLeftNavigationViewController())
                })
            }
        }
    }
    
}

